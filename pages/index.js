import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Layout , { siteTitle } from './components/layout'

import utilStyles from '../styles/utils.module.css'
import { getSortedPostsData } from '../lib/posts';
import Navbar from './components/navbar';

export async function getStaticProps(){
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData
    }
  }
}


export default function Home({ allPostsData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
      
      </section>
    </Layout>
  )
}
