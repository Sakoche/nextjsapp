export default function CustomButton({ children, fontSize, selected }) {
    return   (<button 
    className={`text-gray-700 py-2 px-4 hover:bg-gray-300 hover:bg-opacity-25 font-bold rounded-full transform motion-safe:hover:scale-110 text-${fontSize} ${selected && 'bg-gray-300 bg-opacity-25'}`} >

    {children}
  </button>)
}