import Link from "next/link"
import { Menu } from '@headlessui/react'
import Logo from '../../public/images/logo-omeps-sport-sante-nanterre.svg'
import Login from '../../public/icons/login-sport-sante-nanterre.svg'
import CustomButton from './button';
import CustomMenu from './menu';
import { useState } from "react";

export default function Navbar(){
    const [tabActive, setTabActive]  = useState(0);
    const itemsMenu = ["Sport sur ordonnance", "centre référence ressources", "Inscription Sport à domicile", "Contact"];

    return (<nav className="relative flex items-center justify-between h-20 flex-wrap text-blue-600 p-12  shadow-lg ::after">  

              <Link href='/'><a className="absolute top--10" > <Logo  width={150} height={150} /></a></Link>

              <div className='flex flex-1 justify-end invisible lg:visible '>
                {itemsMenu.map((item, index)=> "centre référence ressources" !== item ? <Link key={index} onClick={() => setTabActive(index)} href='/'><a> <CustomButton  selected={ tabActive === index && "selected"  }>{item}</CustomButton></a></Link> : 
                 <Link key={index} onClick={() => setTabActive(index)} href='/'><a> <CustomMenu  onClick={() => setTabActive(index)}  /></a></Link>)}
              </div>
  
          
              <button className="invisible lg:visible bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full transform motion-safe:hover:scale-110">
                Connexion
              </button>
              
              <div className="block lg:hidden">
                <button className="flex items-center  border rounded text-teal-200 border-teal-400 ">
                  <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                  <title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                </button>
              </div>
    </nav>)
}
