import Head from 'next/head'
import Image from 'next/image'
import styles from './layout.module.css'
import utilStyles from '../../styles/utils.module.css'
import Link from 'next/link'
import Navbar from './navbar'
import Basket from '../../public/images/sport-sante-basket.svg'

const name = 'Your name';
export const siteTitle = 'OMEPS NANTERRE'

export default function Layout({ children, home }) {
    return (
      <div className={styles.container}>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <meta name="description" content="Sport santé sur ordonnance avec l' OMEPS Nanterre"/>
          <meta name="title" content="Sport santé OMEPS"/>
          <meta property="og:image" content="https://sport-sante-omeps.fr/wp-content/uploads//2020/05/LOGO-OMEPS-1.svg" />
          <meta name="og:title" content={siteTitle} />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=0" />

        </Head>
        <Navbar />
        <header className={styles.header +' flex-col lg:flex-row' }>
          <div  className={"p-12 flex flex-col justify-center"}>
            <h1 className={"text-sm lg:text-4xl pb-6 block "}>LA MAISON SPORT SANTÉ OMEPS NANTERRE</h1>
            <blockquote className={`${styles.blockquote} text-sm	lg:text-base block`}>

              {`Les Maisons Sport-Santé ont pour but d'accueillir et d'orienter 
              toutes les personnes souhaitant pratiquer, développer ou reprendre une activité 
              physique et sportive à des fins de santé, de bien-être, quel que soit leur âge. `}
          
            </blockquote>
            <blockquote className={`${styles.blockquote2} invisible lg:visible text-xs lg:text-sm`}>
              {`Elles s'adressent également à des personnes souffrant d'affections longue durée, 
              de maladies chroniques, sur prescription médicale, une activité physique adaptée 
              sécurisée et encadrée par des professionnels`}
            </blockquote>
          </div>
          <div className='flex flex-col h-full w-full items-center justify-center  lg:visible'>
            
            <div className='flex flex-row h-full w-full justify-center'>
              <div className={styles.imageRight2}></div>
              <div className={styles.imageRight3}></div>
            </div>
            <div className='flex flex-row h-full w-full justify-center'>
              <div className={styles.imageRight1 + ' flex flex-column h-full w-full justify-center'}></div>

            </div>
            
          </div>
        </header>
        <main>{children}</main>
        {!home && (
          <div className={styles.backToHome}>
            <Link href="/">
              <a>← Back to home</a>
            </Link>
          </div>
        )}
      </div>
    )
  }