const colors = require('tailwindcss/colors')

module.exports = {
    theme: {
      container: {
        color: colors.red,
        center: true,
        backgroundColor:'yellow',
        padding: '4rem',
      },
      extend: {       
      },
    },
    darkMode: false, // or 'media' or 'class'
    variants: {
      extend: {},
    },
    plugins: [],
    purge: [
      // Use *.tsx if using TypeScript
      './pages/**/*.js',
      './components/**/*.js'
    ]
    // ...
  }